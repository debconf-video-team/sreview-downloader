# SReview Downloader

This script downloads pre-recorded talks from SReview and places them in a
directory on the local host. It will only download the talks that are ready for
playing.

## Set up

```
virtualenv -p python3 pyenv
pyenv/bin/pip install -r requirements.txt
pyenv/bin/python -m sreviewdownload --help
```

It requires the SReview API key and room ID.
