import argparse
import requests
import os
import sys
import logging
import datetime
from pathlib import Path
from dateutil import parser as dateparser

SREVIEW_URL = 'https://sreview.debian.net'
LOGGING_FORMAT = '%(asctime)s - %(levelname)s: %(message)s'
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

def get_option_or_exit(option, env_variable, error_msg, parser):
    if option is None:
        option = os.environ.get(env_variable)
        if option is None:
            print(error_msg, file=sys.stderr)
            parser.print_help()
            sys.exit(1)
    return option

class Downloader:
    ready_states = [
        'preview',
        'publishing',
        'notify_final',
        'finalreview',
        'announcing',
        'done',
    ]
    api_root = '/api/v1'

    def __init__(self, sreview_url, event_id, api_key):
        self.sreview_url = sreview_url
        self.event_id = event_id
        self.api_key = api_key
        self.logger = logging.getLogger(__name__)

    def download(self, directory, room, date):
        self.logger.info(f'Downloading talks...')
        talks = self.get(f'/event/{self.event_id}/talk/list')
        talks = talks.json()
        ready_talks = [t for t in talks if t['state'] in self.ready_states]
        self.logger.info(f'Filtering for room id {room}')
        filtered_talks = [t for t in ready_talks if t['room'] == room]
        filtered_talks = self.filter_date(filtered_talks, date)
        for talk in filtered_talks:
            talk_id = talk['id']
            name = self.get(f'/event/{self.event_id}/talk/{talk_id}/relative_name')
            filename = f'{name.json()}.mp4'
            self.logger.info(f'Handling talk {filename}')
            f = Path(filename)
            local_filename = f.name
            local_path = Path(f'{directory}/{local_filename}')
            if not local_path.exists():
                url = f'{self.sreview_url}/{filename}'
                self.logger.info(f'Downloading {url}...')
                video = requests.get(url)
                with open(local_path, 'wb') as fd:
                    for chunk in video.iter_content(chunk_size=128):
                        fd.write(chunk)

    def filter_date(self, talks, date):
        self.logger.info(f'Filtering for date {date}')
        filtered_talks = []
        for talk in talks:
            start = dateparser.isoparse(talk['starttime'])
            if start.date() == date:
                filtered_talks.append(talk)
        return filtered_talks

    def get(self, endpoint):
        auth_header = {'X-SReview-Key': self.api_key}
        return requests.get(f'{self.sreview_url}{self.api_root}{endpoint}',
                            headers=auth_header)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Pre-record talk copy script for DebConf')
    parser.add_argument('-k', '--key',
                        help='the API key to use for authentication. If not supplied, the environment variable SREVIEW_API_KEY will be used')
    parser.add_argument('-e', '--event',
                        type=int,
                        required=True,
                        help='the ID of the event to download')
    parser.add_argument('-r', '--room',
                        type=int,
                        required=True,
                        help='the ID of the room to download')
    parser.add_argument('-d', '--date',
                        required=True,
                        help='the date to download in ISO format (YYYY-MM-DD)')
    parser.add_argument('directory',
                        help='the directory to save files into')
    parser.add_argument('-v', '--verbose',
                        help='show debug information',
                        action='store_true')
    args = parser.parse_args()
    api_key = get_option_or_exit(args.key,
                                 'SREVIEW_API_KEY',
                                 'No API key provided, unable to continue',
                                 parser)
    date = datetime.datetime.strptime(args.date, '%Y-%m-%d').date()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=LOGGING_FORMAT, datefmt=DATE_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT, datefmt=DATE_FORMAT)

    downloader = Downloader(SREVIEW_URL, args.event, api_key)
    downloader.download(args.directory, args.room, date)
